﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Product_Management.Models;

namespace Product_Management.Controllers
{
    public class ProductController : Controller
    {
        static List<Product> list = new List<Product>();
        // GET: Product
        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Insert(Product product)
        {
            Product obj = new Product();
            obj.ProductName = product.ProductName;
            obj.Description = product.Description;
            list.Add(obj);
            Session["ProductList"] = list;
            return View("Index");
        }

    }


}