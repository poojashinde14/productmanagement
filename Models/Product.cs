﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Product_Management.Models
{
    public class Product
    {
        public string ProductName { get; set; }
        public string Description { get; set; }
    }
}